//
//  DatosPerfil.swift
//  Find Friends
//
//  Created by Mario Abad Martín on 23/3/18.
//  Copyright © 2018 Mario Abad Martín. All rights reserved.
//

import Foundation

class DatosPerfil {
    
    var email: String
    var nickName: String
    var esHombre: Bool
    var fechaNac: Date?
    var ciudad: String
    
    var gustaMusicaBacalao: Bool
    var gustaPaella: Bool
    var gustaBici: Bool
    
    var escalarEverest: Bool
    var bucearConTiburones: Bool
    var leerElQuijote: Bool
    
    init?(email: String){
        self.email = email
        
        self.nickName = ""
        self.esHombre = false
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd/mm/yyyy"
        
        self.fechaNac = dateformatter.date(from: "01/01/1900")
        //self.fechaNac
        self.ciudad = "Madrid"
        
        self.gustaMusicaBacalao = false
        self.gustaPaella = false
        self.gustaBici = false
        
        self.escalarEverest = false
        self.bucearConTiburones = false
        self.leerElQuijote = false
    }
    
    init?(email: String, nickName: String, esHombre: Bool, fechaNac: String, ciudad: String,
          gustaMusicaBacalao: Bool,
          gustaPaella: Bool,
          gustaBici: Bool,
          escalarEverest: Bool,
          bucearConTiburones: Bool,
          leerElQuijote:Bool){
        
        self.email = email
        
        self.nickName = nickName
        self.esHombre = esHombre
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd/MM/yyyy"
        
        self.fechaNac = dateformatter.date(from: fechaNac)
        //self.fechaNac = dateformatter.date(from: "01/01/1900")
        //self.fechaNac
        self.ciudad = ciudad
        
        self.gustaMusicaBacalao = gustaMusicaBacalao
        self.gustaPaella = gustaPaella
        self.gustaBici = gustaBici
        
        self.escalarEverest = escalarEverest
        self.bucearConTiburones = bucearConTiburones
        self.leerElQuijote = leerElQuijote
    }
    
    
    
    
    
}
