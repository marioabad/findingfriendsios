//
//  MapaViewController.swift
//  Find Friends
//
//  Created by Mario Abad Martín on 17/3/18.
//  Copyright © 2018 Mario Abad Martín. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class MapaViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate {


    @IBOutlet weak var miTabla: UITableView!
    @IBOutlet weak var miMapa: MKMapView!
    
    var resutadoMarcado = ["Vacío", "Vacío", "Vacío"]
    let userID = Auth.auth().currentUser?.uid
    var perfil: DatosPerfil?
    var misGustos: [Bool]?
    var misMetas: [Bool]?
    
    let managerHolder = ManagerHolder()
    
    var locman: CLLocationManager {
        return self.managerHolder.locman
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.locman.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        
        if (indexPath.row == 0){
            cell.textLabel!.text = "Nick:"
            cell.detailTextLabel?.text = "\(resutadoMarcado[indexPath.row])"
        } else if (indexPath.row == 1){
            cell.textLabel!.text = "Gustos:"
            cell.detailTextLabel?.text = "\(resutadoMarcado[indexPath.row])"
        } else if (indexPath.row == 2){
            cell.textLabel!.text = "Metas:"
            cell.detailTextLabel?.text = "\(resutadoMarcado[indexPath.row])"
        }
        cell.isUserInteractionEnabled = false
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return " "
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Amigo seleccionado"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //meto mis gustos y mis metas en array para luego poderlo comparar con los gustos y metas de cada usuario conectado
        misGustos = [(perfil?.gustaMusicaBacalao)!, (perfil?.gustaPaella)!, (perfil?.gustaBici)!]
        misMetas = [(perfil?.escalarEverest)!, (perfil?.bucearConTiburones)!, (perfil?.leerElQuijote)!]

        if !CLLocationManager.locationServicesEnabled(){
            self.locman.stopUpdatingLocation()
            return
        }

        miMapa.delegate = self

        managerHolder.checkForLocationAccess()
        
        miMapa.showsUserLocation = true
        miMapa.userTrackingMode = .follow
        
        //Obtengo mi localización
        let loc = CLLocationCoordinate2DMake((locman.location?.coordinate.latitude)!,(locman.location?.coordinate.longitude)!)
        let span = MKCoordinateSpanMake(0.15, 0.15)
        let reg = MKCoordinateRegionMake(loc, span)
        
        miMapa.region = reg
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Obtener y pintar las localizaciones de otros usuarios
        let usersConnectedRef = Database.database().reference(withPath: "usersConnected")
        let datosUsuarioConect = Database.database().reference(withPath: "users")
        usersConnectedRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            let usuariosConectados = snapshot.value as? [String : [String : String]] ?? [:]
            
            print(usuariosConectados)
            
            for (key,_) in usuariosConectados {
                
                if (key != self.userID) {
                    
                    datosUsuarioConect.child(key).observeSingleEvent(of: .value, with: { (snapshot) in
                        // Get user value
                        let value = snapshot.value as? NSDictionary
                        let gustos = [value?["bacalao_music"] as? Bool, value?["paella"] as? Bool,value?["montar_bici"] as? Bool]
                        let metas = [value?["escalar_everest"] as? Bool, value?["bucear_tiburones"] as? Bool,value?["leer_quijote"] as? Bool]
                        
                        self.miMapa.addAnnotation(self.devuelveAnn(identificador: key, coordenadas: usuariosConectados[key]!["position"]!, titulo: usuariosConectados[key]!["nickname"]!, subtitulo: "Buscando amigos", gustos: gustos as! [Bool], metas: metas as! [Bool]))
                        
                    }) { (error) in
                        print(error.localizedDescription)
                    }
                }
            }
        })
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if (view.annotation?.isKind(of: MKUserLocation.self))! {
            
        } else {
            let miAnnotation = view.annotation as! MiAnotacion
            resutadoMarcado[0] = ((miAnnotation.title)!)
            resutadoMarcado[1] = ((miAnnotation.gustos)!)
            resutadoMarcado[2] = ((miAnnotation.metas)!)
            miTabla.reloadData()
        }
    }
    
    class MiAnotacion : NSObject, MKAnnotation {
        dynamic var coordinate: CLLocationCoordinate2D
        var title: String?
        var subtitle: String?
        var ident: String?
        var gustos: String?
        var metas: String?
        init(location coord:CLLocationCoordinate2D){
            self.coordinate = coord
            super.init()
        }
    }
    
    
    func devuelveAnn(identificador: String, coordenadas: String, titulo: String, subtitulo: String, gustos: [Bool], metas: [Bool]) -> MKAnnotation{
        let fullCoordinates = coordenadas
        let fullCoordinatesArr = fullCoordinates.split{$0 == ","}.map(String.init)
        let positionAnn = MiAnotacion(location: CLLocationCoordinate2DMake(Double(fullCoordinatesArr[0])!,Double(fullCoordinatesArr[1])!))
        positionAnn.title = titulo
        positionAnn.subtitle = subtitulo
        positionAnn.ident = identificador
        positionAnn.gustos = comparaGustos(gustos: gustos)
        positionAnn.metas = comparaMetas(metas: metas)
        
        return positionAnn
    }
    
    
    func comparaGustos(gustos: [Bool]) -> String {
        
        var gustosCoincidentes = ""
        for i in 0...2 {
            if (gustos[i] == true && gustos[i] == misGustos![i]) {
                gustosCoincidentes = ("\(gustosCoincidentes) \(Categorias.categorias["Gustos"]![i]) ")
            }
        }
        return gustosCoincidentes
        
    }
    
    func comparaMetas(metas: [Bool]) -> String {
        var metasCoincidentes = ""
        for i in 0...2 {
            if (metas[i] == true && metas[i] == misMetas![i]) {
                metasCoincidentes = ("\(metasCoincidentes) \(Categorias.categorias["Metas y objetivos"]![i]) ")
            }
        }
        return metasCoincidentes
        
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus){
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            self.managerHolder.doThisWhenAuthorized?()
            self.managerHolder.doThisWhenAuthorized = nil
        default:
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        let alert = UIAlertController(title: "Memoria baja", message: "Se va a cerrar", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    class ManagerHolder {
        let locman = CLLocationManager()
        var doThisWhenAuthorized : (() -> ())?
        func  checkForLocationAccess(always: Bool = false, andThen f: (()->())? = nil) {
            // no services? try to get alert
            guard CLLocationManager.locationServicesEnabled() else {
                self.locman.stopUpdatingLocation()
                return
            }
            let status = CLLocationManager.authorizationStatus()
            switch  status {
            case .authorizedAlways, .authorizedWhenInUse:
                f?()
            case .notDetermined:
                self.doThisWhenAuthorized = f
                always ?
                    self.locman.requestAlwaysAuthorization() : self.locman.requestWhenInUseAuthorization()
            case .restricted:
                //No hacer nada
                break
            case .denied:
                //No hacer nada o pedir al usuario que autorice en Configuración
                break

            }
        }
    }
}
