//
//  ViewController.swift
//  Find Friends
//
//  Created by Mario Abad Martín on 28/2/18.
//  Copyright © 2018 Mario Abad Martín. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var imagenFondo: UIImageView!
    @IBOutlet weak var usuarioTF: UITextField!
    @IBOutlet weak var contrasenaTF: UITextField!
    
    var handle: AuthStateDidChangeListenerHandle?
    
    var ref: DatabaseReference!
    var perfil: DatosPerfil?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        imagenFondo.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imgTapped))
        imagenFondo.addGestureRecognizer(tapGesture)

        
        if Auth.auth().currentUser != nil {
            // User is signed in.

            irALaAplicacion()

        } else {
            // No user is signed in.

            print("Usuario no validado")
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.restorationIdentifier == "RIDUsuario"){
            contrasenaTF.becomeFirstResponder()
            
        } else if (textField.restorationIdentifier == "RIDContraseña") {
            validarLogin()
        }
        
        return true
    }
    
    @objc func imgTapped() {
        print("Pulsado")
        self.view.endEditing(true)
    }
    
    func validarLogin() {

        
        Auth.auth().signIn(withEmail: usuarioTF.text ?? "vacio", password: contrasenaTF.text ?? "vacio") { (user, error) in
            if error != nil {
                print(error?.localizedDescription ?? "Desconocido")
                let alert = UIAlertController(title: "Error al validar usuario", message: "Motivo: \(error?.localizedDescription ?? "Desconocido")", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Visto", style: .default, handler: nil))
                
                self.present(alert, animated: true)
                
            }else{
                let user = Auth.auth().currentUser
                
                if let user = user {
                    // The user's ID, unique to the Firebase project.
                    // Do NOT use this value to authenticate with your backend server,
                    // if you have one. Use getTokenWithCompletion:completion: instead.
                    let uid = user.uid
                    let email = user.email
                    let photoURL = user.photoURL
                    
                    print("Identificador: \(uid)")
                    print("Email: \(email ?? "Vacio")")
                    print("URL de foto: \(photoURL?.absoluteString ?? "Vacio")")

                    self.irALaAplicacion()
                }
            }
        }
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            // ...
            print("Usuario validado, el usuario es: \(Auth.auth().currentUser?.email ?? "Vacío")")
        } else {
            // No user is signed in.
            // ...
            print("Usuario no validado")
        }

        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in

        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        print("Me voy, adios")
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    private func cargarDatosPerfil(){
    
    }
    
    private func irALaAplicacion() {
        
        ref = Database.database().reference().child("users")
        
        let userID = Auth.auth().currentUser?.uid
        ref.child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            //let value = snapshot.value as? NSDictionary
            let value = snapshot.value as? [String: AnyObject]
            let email_f = value?["email"] as? String ?? ""
            let nickname_f = value?["nickname"] as? String ?? ""
            let genero_f = value?["genero"] as? String ?? ""
            let fec_nacimiento_f = value?["fec_nacimiento"] as? String ?? ""
            let ciudad_f = value?["ciudad"] as? String ?? ""
            let bacalao_music_f = value?["bacalao_music"] as? Bool ?? false
            let paella_f = value?["paella"] as? Bool ?? false
            let montar_bici_f = value?["montar_bici"] as? Bool ?? false
            let escalar_everest_f = value?["escalar_everest"] as? Bool ?? false
            let bucear_tiburones_f = value?["bucear_tiburones"] as? Bool ?? false
            let leer_quijote_f = value?["leer_quijote"] as? Bool ?? false

            
            self.perfil = DatosPerfil(email: email_f, nickName: nickname_f, esHombre: genero_f == "Hombre" ? true : false, fechaNac: fec_nacimiento_f,
                                      ciudad: ciudad_f, gustaMusicaBacalao: bacalao_music_f, gustaPaella: paella_f,
                                      gustaBici: montar_bici_f, escalarEverest: escalar_everest_f, bucearConTiburones: bucear_tiburones_f, leerElQuijote: leer_quijote_f)
            print("Desde dentro: \(self.perfil?.email ?? "Vacio")")
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let principalVC = storyBoard.instantiateViewController(withIdentifier: "Principal") as! PrincipalViewController
            principalVC.perfil = self.perfil
            print("Este es el email de: \(self.perfil?.email ?? "Vacio")")
            let navigationController = UINavigationController(rootViewController: principalVC)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            self.dismiss(animated: true, completion: nil)
            appDelegate.window?.rootViewController = navigationController
            
            
        }) { (error) in
            print(error.localizedDescription)
        }

    }
    
    @IBAction func botVamos(_ sender: Any) {
        
        validarLogin()
        
    }


}

