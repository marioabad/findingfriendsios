//
//  Categorias.swift
//  Find Friends
//
//  Created by Mario Abad Martín on 19/3/18.
//  Copyright © 2018 Mario Abad Martín. All rights reserved.
//

import Foundation

class Categorias {
    
    static let categorias = [" " : ["Información Personal" ],
                            "Gustos" : ["Música Bacalao","Paella","Montar en bici"],
                            "Metas y objetivos" : ["Escalar el Everest","Bucear con tiburones", "Leer El Quijote"],
                            "Eliminar" : ["Eliminar usuario"]]
    
    
    static let titulos = [" ","Gustos","Metas y objetivos","Eliminar"]
    
    static let ciudades = ["Madrid", "Valencia", "Alicante", "Santander", "Sevilla"]
    
}
