//
//  ConfiguracionTableViewController.swift
//  Find Friends
//
//  Created by Mario Abad Martín on 18/3/18.
//  Copyright © 2018 Mario Abad Martín. All rights reserved.
//

import UIKit
import Firebase

class ConfiguracionTableViewController: UITableViewController {
    
    var ref: DatabaseReference!
    var perfil: DatosPerfil?
    let userID = Auth.auth().currentUser?.uid
    var gustos: [Bool] = []
    var metas: [Bool] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Cargamos el Array de Gustos y Metas
        print("Le gusta el bacalao: \(perfil?.email ?? "No hay nada")")
        gustos = [(perfil?.gustaMusicaBacalao)!,(perfil?.gustaPaella)!, (perfil?.gustaBici)!]
        metas = [(perfil?.escalarEverest)!,(perfil?.bucearConTiburones)!,(perfil?.leerElQuijote)!]
        
        ref = Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return (Categorias.categorias[Categorias.titulos[section]]?.count)!
    
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
            return Categorias.titulos[section]
        
    }
    
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == 0 {
            return " "
        } else if section == 1 {
            return " "
        } else {
            return " "
        }
    }
    

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        cell.textLabel!.text = "\(Categorias.categorias[Categorias.titulos[indexPath.section]]![indexPath.row])"
        
        if (indexPath.section == 0){
            
            cell.accessoryType = .disclosureIndicator
            cell.isSelected = false
            
        } else if (indexPath.section == 1) {

            cell.selectionStyle = .none
            let switchView = UISwitch(frame: CGRect.zero)
            switchView.isOn = gustos[indexPath.row]
            switchView.accessibilityIdentifier = "switch\(indexPath.section)_\(indexPath.row)"
            switchView.addTarget(self, action: #selector(stateChanged), for: UIControlEvents.valueChanged)
            
            cell.accessoryView = switchView
            
        } else if (indexPath.section == 2 ) {
            
            cell.selectionStyle = .none
            let switchView = UISwitch(frame: CGRect.zero)
            switchView.isOn = metas[indexPath.row]
            switchView.accessibilityIdentifier = "switch\(indexPath.section)_\(indexPath.row)"
            switchView.addTarget(self, action: #selector(stateChanged), for: UIControlEvents.valueChanged)
            
            cell.accessoryView = switchView
            
        } else {
            //
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.section == 0){
            
            performSegue(withIdentifier: "datosPersonalesSegue", sender: self)
            
        } else if (indexPath.section == 1 || indexPath.section == 2) {
            
            //print("No hacemos nada")

        } else if (indexPath.section == 3) {
            
            let alertController = UIAlertController (title: "Va a eliminar su usuario", message: "Se perderán todos sus datos ¿Está seguro?", preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alertController.addAction(UIAlertAction(title: "Eliminar", style: .destructive, handler: { _ in
                print("Eliminar")
                
                //Obtenemos las referencias y eliminamos la base de datos
                let usersConnectedRef = Database.database().reference(withPath: "usersConnected")
                let users = Database.database().reference(withPath: "users")
                usersConnectedRef.child(self.userID!).removeValue()
                users.child(self.userID!).removeValue()
                
                //Eliminamos al usuario
                let user = Auth.auth().currentUser
                user?.delete { error in
                    if let error = error {
                        // An error happened.
                        let alert = UIAlertController (title: "Se ha producido un error", message: "\(error.localizedDescription)", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Iniciar sesión", style: .default, handler: {_ in
                            
                            let usersConnectedRef = Database.database().reference(withPath: "usersConnected")
                            usersConnectedRef.child(self.userID!).removeValue()
                            
                            do {
                                try Auth.auth().signOut()
                                
                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let principalVC = storyBoard.instantiateViewController(withIdentifier: "Login") as! ViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = principalVC
                                
                            } catch let signOutError as NSError {
                                print ("Error signing out: %@", signOutError)
                            }
                        }))
                            
                        self.present(alert, animated: true, completion: nil)
                        
                    } else {
                        print("Se desconecta")
                        
                        let usersConnectedRef = Database.database().reference(withPath: "usersConnected")
                        let users = Database.database().reference(withPath: "users")
                        usersConnectedRef.child(self.userID!).removeValue()
                        users.child(self.userID!).removeValue()
                        
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let principalVC = storyBoard.instantiateViewController(withIdentifier: "Login") as! ViewController
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = principalVC
                        
                    }
                }

            }))
            present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func stateChanged(switchState: UISwitch) {
        if switchState.isOn {
            
            print("The Switch \(switchState.accessibilityIdentifier ?? "swVacío") is On")
            switch switchState.accessibilityIdentifier {
            case "switch1_0"?:
                actualizaDatos(dato: "bacalao_music", valor: true)
                perfil?.gustaMusicaBacalao = true
            case "switch1_1"?:
                actualizaDatos(dato: "paella", valor: true)
                perfil?.gustaPaella = true
            case "switch1_2"?:
                actualizaDatos(dato: "montar_bici", valor: true)
                perfil?.gustaBici = true
            case "switch2_0"?:
                actualizaDatos(dato: "escalar_everest", valor: true)
                perfil?.escalarEverest = true
            case "switch2_1"?:
                actualizaDatos(dato: "bucear_tiburones", valor: true)
                perfil?.bucearConTiburones = true
            case "switch2_2"?:
                actualizaDatos(dato: "leer_quijote", valor: true)
                perfil?.leerElQuijote = true
            default:
                print("Error")
            }
            
        } else {
            
            print("The Switch \(switchState.accessibilityIdentifier ?? "swVacío") is Off")
            switch switchState.accessibilityIdentifier {
            case "switch1_0"?:
                actualizaDatos(dato: "bacalao_music", valor: false)
                perfil?.gustaMusicaBacalao = false
            case "switch1_1"?:
                actualizaDatos(dato: "paella", valor: false)
                perfil?.gustaPaella = false
            case "switch1_2"?:
                actualizaDatos(dato: "montar_bici", valor: false)
                perfil?.gustaBici = false
            case "switch2_0"?:
                actualizaDatos(dato: "escalar_everest", valor: false)
                perfil?.escalarEverest = false
            case "switch2_1"?:
                actualizaDatos(dato: "bucear_tiburones", valor: false)
                perfil?.bucearConTiburones = false
            case "switch2_2"?:
                actualizaDatos(dato: "leer_quijote", valor: false)
                perfil?.leerElQuijote = false
            default:
                print("Error")
            }
            
        }
        
    }
    
    func actualizaDatos(dato: String, valor: Bool){
        
        ref.child(dato).setValue(valor)
        
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //
    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "DetVC") as! DetalleViewController
        objVC.perfil = perfil
        
        //print("Le pasao email: \(perfil?.email ?? "OVacio")")
        
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Actions
    
    @IBAction func unwindToConfiguracion(sender: UIStoryboardSegue) {
        
            if let sourceViewController = sender.source as? DetalleViewController, let perfil = sourceViewController.perfil {
                self.perfil = perfil
                print("Estamos en unwind de \(self.perfil?.nickName ?? "No ha venido nada de Detalle")")
            }
            print("Estamos en unwind")
        
    }

}
