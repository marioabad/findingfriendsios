//
//  PrincipalViewController.swift
//  Find Friends
//
//  Created by Mario Abad Martín on 17/3/18.
//  Copyright © 2018 Mario Abad Martín. All rights reserved.
//

import UIKit
import Firebase
import MapKit

class PrincipalViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var usuarioLabel: UILabel!
    
    var handle: AuthStateDidChangeListenerHandle?
    var perfil: DatosPerfil?
    var ref: DatabaseReference!
    
    let userID = Auth.auth().currentUser?.uid
    
    let managerHolder = ManagerHolder()
    var locman: CLLocationManager {
        return self.managerHolder.locman
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.locman.delegate = self
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if Auth.auth().currentUser != nil {
            // User is signed in.

            usuarioLabel.text = "\(perfil?.email ?? "Vacío")"

        } else {
            // No user is signed in.

            print("Principal Usuario no validado")
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            // ...
            
            if user?.uid == nil {
                //print("Soy user \(user?.uid ?? "No soy user porque me he desconectado")")
            }
            
            //print("Ha cambiado ek estado y me he ido")
        }
        
        //Actualizo perfil
        ref = Database.database().reference().child("users")
        
        ref.child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            //let value = snapshot.value as? NSDictionary
            let value = snapshot.value as? [String: AnyObject]
            let email_f = value?["email"] as? String ?? ""
            let nickname_f = value?["nickname"] as? String ?? ""
            let genero_f = value?["genero"] as? String ?? ""
            let fec_nacimiento_f = value?["fec_nacimiento"] as? String ?? ""
            let ciudad_f = value?["ciudad"] as? String ?? ""
            let bacalao_music_f = value?["bacalao_music"] as? Bool ?? false
            let paella_f = value?["paella"] as? Bool ?? false
            let montar_bici_f = value?["montar_bici"] as? Bool ?? false
            let escalar_everest_f = value?["escalar_everest"] as? Bool ?? false
            let bucear_tiburones_f = value?["bucear_tiburones"] as? Bool ?? false
            let leer_quijote_f = value?["leer_quijote"] as? Bool ?? false
            
        
            self.perfil?.email = email_f
            self.perfil?.nickName = nickname_f
            self.perfil?.esHombre = genero_f == "Hombre" ? true : false
            
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "dd/MM/yyyy"
            self.perfil?.fechaNac = dateformatter.date(from: fec_nacimiento_f)
            
            self.perfil?.ciudad = ciudad_f
            self.perfil?.gustaMusicaBacalao = bacalao_music_f
            self.perfil?.gustaPaella = paella_f
            self.perfil?.gustaBici = montar_bici_f
            self.perfil?.escalarEverest = escalar_everest_f
            self.perfil?.bucearConTiburones = bucear_tiburones_f
            self.perfil?.leerElQuijote = leer_quijote_f
            
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
        let usersConnectedRef = Database.database().reference(withPath: "usersConnected")
        
        let connectedRef = Database.database().reference(withPath: ".info/connected")
        connectedRef.observe(.value, with: { snapshot in
            // only handle connection established (or I've reconnected after a loss of connection)
            guard let connected = snapshot.value as? Bool, connected else { return }
            
            //Añadimos mi posición a la base de datos
            let con = usersConnectedRef.child(self.userID!)
            con.onDisconnectRemoveValue()
            
            if !CLLocationManager.locationServicesEnabled(){
                self.locman.stopUpdatingLocation()
                return
            }
            
            self.managerHolder.checkForLocationAccess()
            
            var currentLocation : CLLocation!
            currentLocation = nil
            if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
                
                currentLocation = self.locman.location
                
                con.child("position").setValue("\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)")
                con.child("nickname").setValue("\(self.perfil?.nickName ?? "Sin nickname")")
                //lastOnlineRef.onDisconnectSetValue(ServerValue.timestamp())
                
            }
        })
    }
    
    // MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == "mapSegue"){
            
            //Comprobar que tiene los permisos de localización
            if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
                return true
            } else {
                print("Solicitar permisos")
                
                let alertController = UIAlertController (title: "Sin localización", message: "Para localizar amigos debes conceder antes permiso de localización ¿Quieres ir a ajustes?", preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: "Ir a ajustes", style: .default) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                alertController.addAction(cancelAction)
                
                present(alertController, animated: true, completion: nil)
                return false
            }
        }
        return true
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "mostrarConfiguracion") {

            print("Preparando el paso de objeto: \(perfil?.email ?? "aqui no hay nada que pasar")")
            
            let confiTVC = segue.destination as! ConfiguracionTableViewController
            
            confiTVC.perfil = perfil
            print("Pasado de objeto: \(confiTVC.perfil?.email ?? "nada pasado")")
        
        } else if (segue.identifier == "mapSegue"){
            
                let confiTVC = segue.destination as! MapaViewController
                confiTVC.perfil = perfil
        }
    }
    
    
    @IBAction func desconectar(_ sender: Any) {
        
        let usersConnectedRef = Database.database().reference(withPath: "usersConnected")
        usersConnectedRef.child(userID!).removeValue()
        usersConnectedRef.removeAllObservers()
        
         do {
         try Auth.auth().signOut()
         } catch let signOutError as NSError {
         print ("Error signing out: %@", signOutError)
         }
        
        
        print("\(Auth.auth().currentUser?.email ?? "Vacío")")
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let principalVC = storyBoard.instantiateViewController(withIdentifier: "Login") as! ViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = principalVC
        
    }
    
    class ManagerHolder {
        let locman = CLLocationManager()
        var doThisWhenAuthorized : (() -> ())?
        func  checkForLocationAccess(always: Bool = false, andThen f: (()->())? = nil) {
            // no services? try to get alert
            guard CLLocationManager.locationServicesEnabled() else {
                self.locman.stopUpdatingLocation()
                return
            }
            let status = CLLocationManager.authorizationStatus()
            switch  status {
            case .authorizedAlways, .authorizedWhenInUse:
                f?()
            case .notDetermined:
                self.doThisWhenAuthorized = f
                always ?
                    self.locman.requestAlwaysAuthorization() : self.locman.requestWhenInUseAuthorization()
            case .restricted:
                //No hacer nada
                break
            case .denied:
                //No hacer nada o pedir al usuario que autorice en Configuración

                break
                
            }
        }
    }
}
