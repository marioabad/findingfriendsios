//
//  AltaUsuarioViewController.swift
//  Find Friends
//
//  Created by Mario Abad Martín on 9/3/18.
//  Copyright © 2018 Mario Abad Martín. All rights reserved.
//

import UIKit
import Firebase
import os.log

class AltaUsuarioViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var usuarioTxt: UITextField!
    @IBOutlet weak var contraseñaTxt: UITextField!
    @IBOutlet weak var enviarBot: UIButton!
    @IBOutlet weak var volverBot: UIBarButtonItem!
    
    
    var handle: AuthStateDidChangeListenerHandle?
    var datosPerfil: DatosPerfil?
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imgTapped))
        view.addGestureRecognizer(tapGesture)
        
        usuarioTxt.delegate = self
        contraseñaTxt.delegate = self
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func imgTapped() {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.restorationIdentifier == "RIDUsuarioAlta"){
            contraseñaTxt.becomeFirstResponder()
            
        } else if (textField.restorationIdentifier == "RIDContraseñaAlta") {
            validarYEnviarDatos()
        }
        
        return true
    }
    
    func validarYEnviarDatos() {
        print("Usuario \(usuarioTxt.text ?? "vacio")")
        print("Contraseña \(contraseñaTxt.text ?? "vacio")")
        
        Auth.auth().createUser(withEmail: usuarioTxt.text ?? "vacio", password: contraseñaTxt.text ?? "vacio") { (user, error) in
            if error != nil {
                print(error?.localizedDescription ?? "Desconocido")
                let alert = UIAlertController(title: "Error al crear usuario", message: "Motivo: \(error?.localizedDescription ?? "Desconocido")", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Visto", style: .default, handler: nil))
                
                self.present(alert, animated: true)
                
            }else{
                print ("Usuario creado")
                
                //Alimentamos campos en la BBDD
                let usuario = ["email": (Auth.auth().currentUser?.email) ?? "vacio",
                               "nickname": "",
                               "genero": "Hombre",
                               "fec_nacimiento": "01/01/1975",
                               "ciudad": "Madrid",
                               "bacalao_music": false,
                               "paella": false,
                               "montar_bici": false,
                               "escalar_everest": false,
                               "bucear_tiburones": false,
                               "leer_quijote": false
                    ] as [String : Any]
                
                
                self.ref = Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!)
                self.ref.setValue(usuario)
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let principalVC = storyBoard.instantiateViewController(withIdentifier: "Principal") as! PrincipalViewController
                principalVC.perfil = DatosPerfil(email: "\(Auth.auth().currentUser?.email ?? "Vacío")")
                let navigationController = UINavigationController(rootViewController: principalVC)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                self.dismiss(animated: true, completion: nil)
                appDelegate.window?.rootViewController = navigationController
            }
            
        }

        self.view.endEditing(true)
        
    }
    
    @IBAction func enviarDatos(_ sender: Any) {
        
        validarYEnviarDatos()
    }
    
    @IBAction func volver(_ sender: Any) {
         dismiss(animated: true, completion: nil)
    }
    

    override func viewWillAppear(_ animated: Bool) {
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in

        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        Auth.auth().removeStateDidChangeListener(handle!)

    }
    
}
