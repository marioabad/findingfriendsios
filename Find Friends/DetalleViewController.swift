//
//  DetalleViewController.swift
//  Find Friends
//
//  Created by Mario Abad Martín on 23/3/18.
//  Copyright © 2018 Mario Abad Martín. All rights reserved.
//

import UIKit
import Firebase

import os.log



class DetalleViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource{
    

    @IBOutlet weak var nickNameTF: UITextField!
    @IBOutlet weak var generoSC: UISegmentedControl!
    @IBOutlet weak var fechaNacimientoDP: UIDatePicker!
    @IBOutlet weak var ciudadPV: UIPickerView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var ref: DatabaseReference!
    var perfil: DatosPerfil?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Connect data:
        self.ciudadPV.delegate = self
        self.ciudadPV.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("Perfil \(perfil?.nickName ?? "No me han pasado nada")")
        
        nickNameTF.text = self.perfil?.nickName
        generoSC.selectedSegmentIndex = (self.perfil?.esHombre)! ? 1 : 0
        
        
        fechaNacimientoDP.setDate((self.perfil?.fechaNac)!, animated: false)
        ciudadPV.selectRow(self.devuelveCiudad(ciudad: (self.perfil?.ciudad)!), inComponent: 0, animated: false)
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.restorationIdentifier == "NickName"){
            self.view.endEditing(true)
        }
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Categorias.ciudades.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Categorias.ciudades[row]
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        
        // Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            print("Entra en Segue no save")
            return
        }
        

        perfil?.nickName = nickNameTF.text!
        perfil?.esHombre = generoSC.selectedSegmentIndex == 1 ? true : false
        perfil?.fechaNac = fechaNacimientoDP.date
        perfil?.ciudad = Categorias.ciudades[ciudadPV.selectedRow(inComponent: 0)]
        
        print("Entra en Segue: \(segue.identifier ?? "Segue desconocido")")
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd/MM/yyyy"
        
        let datosUsuario = ["nickname": perfil?.nickName ?? "Nick vacío",
                            "genero": (perfil?.esHombre)! ? "Hombre" : "Mujer",
                            "fec_nacimiento": dateformatter.string(from: (perfil?.fechaNac)!),
                            "ciudad": perfil?.ciudad ?? "Nimguna ciudad"
            ] as [String : String]
        
        
        ref = Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!)
        //ref.setValue(datosUsuario)
        ref.updateChildValues(datosUsuario)
        
    }
   
    
    func devuelveCiudad(ciudad: String) -> Int {
        if (ciudad == "Madrid") {
            return 0
        } else if (ciudad == "Valencia") {
            return 1
        } else if (ciudad == "Alicante") {
            return 2
        } else if (ciudad == "Santander") {
            return 3
        } else if (ciudad == "Sevilla") {
            return 4
        } else {
            return 0
        }
    }
    
}
